<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\PostController::class, 'index'])->name('posts');
Route::get('/create', [App\Http\Controllers\PostController::class, 'create'])->name('create');
Route::post('/store', [App\Http\Controllers\PostController::class, 'store'])->name('store');


Route::get('/posts/{slug}', [App\Http\Controllers\PostController::class, 'show'])->name('show')->where('slug', '[A-Za-z0-9-_]+');
Route::get('/posts/{slug}/edit', [App\Http\Controllers\PostController::class, 'edit'])->name('edit')->where('slug', '[A-Za-z0-9-_]+');
Route::post('/posts/{slug}/update', [App\Http\Controllers\PostController::class, 'update'])->name('update')->where('slug', '[A-Za-z0-9-_]+');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

@extends('layouts.app')

@section('title',__('Cratea a new Post') )

@section('content')
        <div class="row">
            <form action="{{ route('store') }}" method="POST" class="col-12 needs-validation" novalidate>
                @csrf
            <div class="form-group">
                <label for="Name">{{ __('Title') }}</label>
                <input type="text" name="title" placeholder="Name" class="form-control" required>
                <span class="text-danger">{{ $errors->first('title') }}</span>
            </div>
        <div class="form-group required">
            <label for="Services">{{ __('Content') }}</label>
            <textarea class="ckeditor form-control" name="body" required></textarea>
            <span class="text-danger">{{ $errors->first('body') }}</span>
        </div>
        <button class="btn btn-success float-right">{{ __('Save') }}</button>
    </form>
    </div>
@endsection

@extends('layouts.app')

@section('title',__('Display the Post') )
@section('content')
    @if($post)
        <div>
            <h1 class="text-center">{{$post->title}}</h1>
            <div>{!! $post->body !!}</div>
            <span>{{ __('Author') }}: {{$post->author->name}}</span>
        </div>
    @endif
@endsection

@extends('layouts.app')

@section('title',__('All Posts') )

@section('content')
    <div class="row">
        <div class="card-deck">
            @foreach($posts as $post)
                <div class="card">
                    <img class="card-img-top" src="https://via.placeholder.com/300" alt="Card image cap">
                    <div class="card-body">
                        <a href="/posts/{{$post->slug}}"><h5 class="card-title">{{$post->title}}</h5></a>
                        <p class="card-text">{{ strip_tags($post->excerpt())}}</p>
                        @auth
                            <a href="/posts/{{$post->slug}}/edit"><button class="btn btn-primary">{{ __('Edit') }}</button></a>
                        @endauth
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="d-flex justify-content-center mt-3">
        {!! $posts->links() !!}
    </div>
    @auth
        <a class="btn btn-success" href="{{ route('create') }}">
            {{ __('Create Post') }}
        </a>
    @endauth
@endsection

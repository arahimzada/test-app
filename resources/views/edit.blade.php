@extends('layouts.app')

@section('title',__('Update Post') )

@section('content')
        <div class="row">
            <form action="{{ route('update', [$post->slug]) }}" method="POST" class="col-12 needs-validation" novalidate>
                @csrf
            <div class="form-group">
                <label for="Name">{{ __('Title') }}</label>
                <input type="text" name="title" placeholder="Name" class="form-control" value="{{$post->title}}" required>
                <span class="text-danger">{{ $errors->first('title') }}</span>
            </div>
        <div class="form-group required">
            <label for="Services">{{ __('Content') }}</label>
            <textarea class="ckeditor form-control" name="body" required>{!! $post->body !!}</textarea>
            <span class="text-danger">{{ $errors->first('body') }}</span>
        </div>
        <button class="btn btn-success float-right">{{ __('Update') }}</button>
    </form>
    </div>
@endsection

<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->paginate(4);
        return view('start', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);

        $author = auth()->user();
        $post= new Post();
        if ($request->has('title')) {
            $title = $request->get('title');
            $post->title = $title;
            $post->slug = SlugService::createSlug(Post::class, 'slug', $title);
        }
        if ($request->has('body')) {
            $post->body = $request->get('body');
        }
        $post->author()->associate($author);

        $post->save();

        return redirect()->intended(route('posts'));
    }

    /**
     * @param $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $slugString = $slug;
        $post = Post::findBySlug($slugString);
        return view('show')->withPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $slugString = $request->slug;
        $post = Post::findBySlug($slugString);
        return view('edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
        $slugString = $request->slug;
        $post = Post::findBySlug($slugString);

        if ($request->has('title')) {
            $title = $request->get('title');
            $post->title = $title;
            $post->slug = SlugService::createSlug(Post::class, 'slug', $title);
        }
        if ($request->has('body')) {
            $post->body = $request->get('body');
        }
        $post->update();

        return redirect()->intended(route('posts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
